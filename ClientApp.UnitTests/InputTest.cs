using Grpc.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Moq;
using Studio8Test.GrpcServices;
using Studio8Test.Input;
using Studio8Test.Tools.Calculation;
using System;
using System.Threading;
using Xunit;

namespace Studio8Test.ClientApp.UnitTests
{
    public class InputTest
    {
        private IHost CreateHost(ICalculator calculator, Action<ConnectionSettings> configureConnectionSettings = null, Action<IServiceCollection> configureServices = null)
        {
            return Host.CreateDefaultBuilder()
                   .UseContentRoot(AppDomain.CurrentDomain.BaseDirectory)
                   .ConfigureServices(s => s.AddSingleton(calculator))
                   .ConfigureServices(s => configureServices?.Invoke(s))
                   .AddClient(configureConnectionSettings ?? (connectionSettings => connectionSettings.ServerAddress = "http://locahost:9999"))
                   .Build();
        }

        [Fact]
        public void Should_Request_Input_On_Start()
        {
            //Arrange
            using var host = CreateHost(Mock.Of<ICalculator>());

            var requestInputMres = new ManualResetEventSlim(false);
            var clientAppViewMock = new Mock<IClientAppView>();
            clientAppViewMock.Setup(m => m.RequestInput(It.IsAny<SimpleMathFunctionInputRequest>()))
                .Returns(new SimpleMathFunction { Number = 54, Function = "cube" })
                .Callback(() => requestInputMres.Set());

            //Act
            ThreadPool.QueueUserWorkItem(w => host.Run());
            var app = host.Services.GetRequiredService<IClientApp>();
            app.Start(clientAppViewMock.Object);

            //Assert
            Assert.True(requestInputMres.Wait(TimeSpan.FromSeconds(3)), "Failed to wait for a method call within the allotted time");
        }

        [Fact]
        public void Should_Calculate_Odd_Number_Local()
        {
            //Arrange
            var calculateMres = new ManualResetEventSlim(false);
            var calculatorMock = new Mock<ICalculator>();
            calculatorMock.Setup(m => m.Calculate(It.IsAny<double>(), It.IsAny<string>()))
                .Callback(() => calculateMres.Set());

            using var host = CreateHost(calculatorMock.Object);
            
            var clientAppViewMock = new Mock<IClientAppView>();
            clientAppViewMock.Setup(m => m.RequestInput(It.IsAny<SimpleMathFunctionInputRequest>()))
                .Returns(new SimpleMathFunction { Number = 55, Function = "cube" });

            //Act
            ThreadPool.QueueUserWorkItem(w => host.Run());
            var app = host.Services.GetRequiredService<IClientApp>();
            app.Start(clientAppViewMock.Object);

            //Assert
            Assert.True(calculateMres.Wait(TimeSpan.FromSeconds(3)), "Failed to wait for calculate within the allotted time");
        }

        [Fact]
        public void Should_Calculate_Even_Number_Remote()
        {
            //Arrange
            var calculatorMock = new Mock<ICalculator>();

            using var host = CreateHost(calculatorMock.Object);

            var clientAppViewMock = new Mock<IClientAppView>();
            clientAppViewMock.Setup(m => m.RequestInput(It.IsAny<SimpleMathFunctionInputRequest>()))
                .Returns(new SimpleMathFunction { Number = 54, Function = "cube" });

            var showResultMres = new ManualResetEventSlim(false);
            clientAppViewMock.Setup(m => m.ShowResult(It.IsAny<double>()))
                .Callback(() => showResultMres.Set());

            //Act
            ThreadPool.QueueUserWorkItem(w => host.Run());
            var app = host.Services.GetRequiredService<IClientApp>();
            app.Start(clientAppViewMock.Object);

            //Assert
            Assert.True(showResultMres.Wait(TimeSpan.FromSeconds(3)), "Failed to wait for result within the allotted time");
            calculatorMock.Verify(m => m.Calculate(It.IsAny<double>(), It.IsAny<string>()), Times.Never(), "Local calculation for the odd numbers should not be called even once");
            calculatorMock.Verify(m => m.Calculate(It.IsAny<string>()), Times.Never(), "Local calculation for the odd numbers should not be called even once");
        }
    }
}
