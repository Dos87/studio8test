﻿namespace Studio8Test
{
    public class ConnectionSettings
    {
        public string? ServerAddress { get; set; }
        public bool Http2UnencryptedSupport { get; set; }
    }
}
