﻿using Studio8Test.Input;

namespace Studio8Test
{
    public interface IClientAppView
    {
        T RequestInput<T>(IInputRequest<T> inputRequest);
        void ShowResult(double result);
        void ShowError(string message);
    }
}
