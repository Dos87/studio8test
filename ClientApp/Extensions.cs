using System;
using System.Linq;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Studio8Test.Tools.Calculation;

namespace Studio8Test
{
    public static class Extensions
    {
        /// <summary>You can use the "Connection" section in appsettings.json and/or pass the settings in this method</summary>
        /// <param name="hostBuilder">Instance of IHostBuilder</param>
        /// <param name="configureConnectionSettings">The configuration of the settings</param>
        public static IHostBuilder AddClient(this IHostBuilder hostBuilder, Action<ConnectionSettings>? configureConnectionSettings = null)
        {
            return hostBuilder.ConfigureServices((ctx, s) =>
            {
                s.Configure<ConnectionSettings>(ctx.Configuration.GetSection("Connection"));
                if (configureConnectionSettings != null)
                    s.Configure(configureConnectionSettings);
                s.AddSingleton<IClientApp, ClientApp>();
                s.AddValidatorsFromAssemblyContaining<ClientApp>();

                if (!s.Any(x => x.ServiceType == typeof(ICalculator)))
                    throw new NotImplementedException($"You must register the implementation for {nameof(ICalculator)}");
            });
        }

        public static IHostBuilder AddClient<TCalculator>(this IHostBuilder hostBuilder, Action<ConnectionSettings>? configureConnectionSettings = null)
            where TCalculator : class, ICalculator
            => hostBuilder
                .ConfigureServices((ctx, s) => s.AddTransient<ICalculator, TCalculator>())
                .AddClient(configureConnectionSettings);
    }
}