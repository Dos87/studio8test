﻿namespace Studio8Test.Input
{
    public interface IInputRequest<T>
    {
        string Message { get; }
        T Parse(string input);
    }
}