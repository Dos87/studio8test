﻿using Studio8Test.GrpcServices;

namespace Studio8Test.Input
{
    public class SimpleMathFunctionInputRequest : IInputRequest<SimpleMathFunction>
    {
        public string Message => "Inter a number with function";

        public SimpleMathFunction Parse(string input)
        {
            var parts = input.Split(' ', System.StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 2 && double.TryParse(parts[0], out var m))
                return new SimpleMathFunction { Number = m, Function = parts[1] };
            throw new InputRequestParsingException("Incorrect input. You must use the following format: {number} {function}");
        }
    }
}
