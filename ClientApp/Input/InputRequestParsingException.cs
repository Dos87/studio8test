﻿using System;

namespace Studio8Test.Input
{
    public class InputRequestParsingException : Exception
    {
        public InputRequestParsingException(string message) : base(message)
        {
        }

        public InputRequestParsingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
