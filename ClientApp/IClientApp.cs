﻿namespace Studio8Test
{
    public interface IClientApp
    {
        void Start(IClientAppView view);
    }
}
