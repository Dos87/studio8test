﻿using FluentValidation;
using Studio8Test.GrpcServices;
using System.Linq;

namespace Studio8Test.Validators
{
    public class SimpleMathFunctionValidator : AbstractValidator<SimpleMathFunction>
    {
        private static readonly string[] _awailableFunctions = new[] { "cube", "square" };

        public SimpleMathFunctionValidator()
        {
            RuleFor(f => f.Number)
                .GreaterThanOrEqualTo(1)
                .WithMessage("The number should be greater than or equal to 1")
                .LessThanOrEqualTo(100)
                .WithMessage("The number should be less than or equal to 100")
                .Must(n => (long)n % 2 != 0)
                .WithMessage("The number must be odd");

            RuleFor(f => f.Function)
                .Must(f => _awailableFunctions.Any(af => af.Equals(f, System.StringComparison.InvariantCultureIgnoreCase)))
                .WithMessage("The word should be one of the following: " + string.Join(", ", _awailableFunctions));
        }
    }
}
