﻿using System;
using System.Linq;
using System.Threading;
using FluentValidation;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Studio8Test.GrpcServices;
using Studio8Test.Input;
using Studio8Test.Tools.Calculation;

namespace Studio8Test
{
    internal class ClientApp : IClientApp
    {
        private readonly ICalculator _calculator;
        private readonly IValidator<SimpleMathFunction> _validator;
        private readonly ILogger<ClientApp> _logger;
        private readonly Thread _inputThread;
        private CalculationGrpc.CalculationGrpcClient? _transport;
        private IClientAppView? _view;

        public ClientApp(
            ICalculator calculator,
            IValidator<SimpleMathFunction> validator,
            ILogger<ClientApp> logger,
            IOptionsMonitor<ConnectionSettings> connectionSettings,
            IHostApplicationLifetime hostApplicationLifetime)
        {
            _calculator = calculator;
            _validator = validator;
            _logger = logger;
            _inputThread = new Thread(AppLoop) {IsBackground = true};
            hostApplicationLifetime.ApplicationStopping.Register(_inputThread.Interrupt);
            connectionSettings.OnChange(Setup);
            Setup(connectionSettings.CurrentValue);
        }

        public void Start(IClientAppView view)
        {
            _view = view;
            _inputThread.Start();
        }

        private void AppLoop()
        {
            while (true)
            {
                try
                {
                    var input = _view!.RequestInput(new SimpleMathFunctionInputRequest());
                    double result;
                    if ((long)input.Number % 2 == 0)
                    {
                        result = _transport!.Calculate(input).Result;
                    }
                    else
                    {
                        var validationResult = _validator.Validate(input);
                        if (!validationResult.IsValid)
                            throw new ValidationException(validationResult.Errors);
                        result = _calculator.Calculate(input.Number, input.Function);
                    }
                    _view.ShowResult(result);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch (ValidationException exc)
                {
                    _view!.ShowError(string.Join('\n', exc.Errors.Select(e => e.ErrorMessage)));
                }
                catch (RpcException exc) when (exc.StatusCode == StatusCode.InvalidArgument)
                {
                    _view!.ShowError(exc.Status.Detail);
                }
                catch (RpcException exc) when (exc.StatusCode == StatusCode.Internal)
                {
                    _view!.ShowError("Server error: " + exc.Status.Detail);
                }
                catch (Exception exc)
                {
                    _logger.LogError(exc, "Error in application loop");
                    _view!.ShowError(exc.Message);
                }
            }
        }

        private void Setup(ConnectionSettings connectionSettings)
        {
            if (string.IsNullOrWhiteSpace(connectionSettings.ServerAddress))
                throw new ArgumentException($"You must specify \"{nameof(ConnectionSettings)}.{nameof(connectionSettings.ServerAddress)}\" in the settings", nameof(connectionSettings));

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", connectionSettings.Http2UnencryptedSupport);

            var channel = GrpcChannel.ForAddress(connectionSettings.ServerAddress);
            _transport = new CalculationGrpc.CalculationGrpcClient(channel);
        }
    }
}