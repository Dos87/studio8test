using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace Studio8Test.Tools.Calculation.UnitTests
{
    public class CalculatorTest
    {
        [Fact]
        public void Should_Calculate_The_Square()
        {
            //Arrange
            var calculator = new Calculator(Mock.Of<ILogger<Calculator>>());
            var number = 55;
            var function = "square";

            //Act
            var result = calculator.Calculate(number, function);

            //Assert
            Assert.Equal(Math.Pow(number, 2), result);
        }

        [Fact]
        public void Should_Calculate_The_Cube()
        {
            //Arrange
            var calculator = new Calculator(Mock.Of<ILogger<Calculator>>());
            var number = 55;
            var function = "cube";

            //Act
            var result = calculator.Calculate(number, function);

            //Assert
            Assert.Equal(Math.Pow(number, 3), result);
        }
    }
}
