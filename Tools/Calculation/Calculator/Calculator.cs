﻿using System;
using Jace;
using Microsoft.Extensions.Logging;

namespace Studio8Test.Tools.Calculation
{
    public class Calculator : ICalculator
    {
        private readonly CalculationEngine _engine = new CalculationEngine();
        private readonly ILogger<Calculator> _logger;

        public Calculator(ILogger<Calculator> logger)
        {
            _logger = logger;
            _engine.AddFunction("square", a => Math.Pow(a, 2));
            _engine.AddFunction("cube", a => Math.Pow(a, 3));
        }

        public double Calculate(string expression)
        {
            _logger.LogDebug("Calculating expression \"{EXPRESSION}\"...", expression);
            var result = _engine.Calculate(expression);
            _logger.LogDebug("Result of calculating the expression \"{EXPRESSION}\": {EXPRESSION_RESULT}", expression, result);
            return result;
        }

        public double Calculate(double number, string function)
            => Calculate($"{number} {function}");
    }
}