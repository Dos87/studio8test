﻿namespace Studio8Test.Tools.Calculation
{
    public interface ICalculator
    {
        double Calculate(string expression);
        double Calculate(double number, string function);
    }
}
