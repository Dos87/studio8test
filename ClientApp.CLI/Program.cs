﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using Studio8Test.Tools.Calculation;

namespace Studio8Test.ClientApp.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            using var host = Host.CreateDefaultBuilder(args)
                .UseContentRoot(AppDomain.CurrentDomain.BaseDirectory)
                .AddSerilog()
                .AddClient<Calculator>()
                .ConfigureServices(s => s.AddHostedService<CliClientAppView>())
                .Build();
            try
            {
                host.Start();
            }
            catch (Exception exc)
            {
                var logger = host.Services.GetRequiredService<ILogger<Program>>();
                logger.LogCritical(exc, "Unhandled exception");
                var view = host.Services.GetRequiredService<IClientAppView>();
                view.ShowError("Unhandled exception! " + exc.Message);
                Console.ReadKey();
            }
            host.WaitForShutdown();
        }
    }

    internal static class HostBuilderExtensions
    {
        public static IHostBuilder AddSerilog(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureLogging((ctx, loggingBuilder) =>
            {
                var cfg = new LoggerConfiguration()
                    .ReadFrom.Configuration(ctx.Configuration)
                    .Enrich.FromLogContext()
                    .Enrich.WithProperty("ApplicationContext", "ClientApp")
                    .WriteTo.Console(
                        theme: AnsiConsoleTheme.Code,
                        outputTemplate: "[{Timestamp:HH:mm:ss.fff} {Level:u3} {SourceContext}] {Message:lj}{NewLine}{Exception}");
                
                var seqServerUrl = ctx.Configuration["Serilog:Seq:ServerUrl"];
                if (!string.IsNullOrWhiteSpace(seqServerUrl))
                    cfg.WriteTo.Seq(seqServerUrl, apiKey: ctx.Configuration["Serilog:Seq:ApiKey"], compact: true);

                Log.Logger = cfg.CreateLogger();
                loggingBuilder.ClearProviders();
                loggingBuilder.AddSerilog(dispose: true);
            });
        }
    }
}