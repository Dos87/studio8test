using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Studio8Test.Input;

namespace Studio8Test.ClientApp.CLI
{
    internal class CliClientAppView : IClientAppView, IHostedService
    {
        private readonly IClientApp _clientApp;
        private readonly ILogger<CliClientAppView> _logger;

        public CliClientAppView(IClientApp clientApp, ILogger<CliClientAppView> logger)
        {
            _clientApp = clientApp;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
            => Task.Factory.StartNew(
                () => Task.Delay(1000, cancellationToken)
                .ContinueWith(t => Console.WriteLine(), cancellationToken)
                .ContinueWith(t => _clientApp.Start(this), TaskContinuationOptions.OnlyOnRanToCompletion), cancellationToken);

        public Task StopAsync(CancellationToken cancellationToken)
            => Task.CompletedTask;

        public T RequestInput<T>(IInputRequest<T> inputRequest)
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(inputRequest.Message);
                Console.ResetColor();
                try
                {
                    var input = Console.ReadLine();
                    if (input is null) //Application closing...
                        throw new OperationCanceledException();
                    return inputRequest.Parse(input);
                }
                catch (InputRequestParsingException exc)
                {
                    _logger.LogWarning(exc, "Parsing error");
                    ShowError("Parsing error: " + exc.Message);
                }
            }
        }

        public void ShowError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
            Console.WriteLine();
        }

        public void ShowResult(double result)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Result: " + result);
            Console.ResetColor();
            Console.WriteLine();
        }
    }
}