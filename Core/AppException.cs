using System;
using System.Text;

namespace Studio8Test
{
    public class AppException : Exception
    {
        private readonly string _structuredMessage;
        private readonly object[] _args;
        
        public AppException(Exception? innerException, string structuredMessage, params object[] args)
            :base(BuildMessage(structuredMessage, args), innerException)
        {
            _structuredMessage = structuredMessage;
            _args = args;
        }

        public AppException(string structuredMessage, params object[] args)
            : this(null, structuredMessage, args) { }

        private static string BuildMessage(string structuredMessage, object[] args)
        {
            if (args.Length == 0)
                return structuredMessage;

            var sb = new StringBuilder();
            var lastIndexOfClosingBracket = 0;
            foreach (var arg in args)
            {
                var indexOfOpeningBracket = structuredMessage.IndexOf('{', lastIndexOfClosingBracket);
                if (indexOfOpeningBracket == -1)
                {
                    sb.Append("[Message building error. Missing '{']");
                    sb.Append(structuredMessage, lastIndexOfClosingBracket, structuredMessage.Length - lastIndexOfClosingBracket);
                    break;
                }

                sb.Append(structuredMessage, lastIndexOfClosingBracket, indexOfOpeningBracket - lastIndexOfClosingBracket);
                sb.Append(arg);
                lastIndexOfClosingBracket = structuredMessage.IndexOf('}', indexOfOpeningBracket);
                if (lastIndexOfClosingBracket == -1)
                {
                    sb.Append("[Message building error. Missing '}']");
                    sb.Append(structuredMessage, lastIndexOfClosingBracket, structuredMessage.Length - lastIndexOfClosingBracket);
                    break;
                }
            }
            return sb.ToString();
        }
    }
}