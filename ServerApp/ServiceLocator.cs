﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Studio8Test
{
    //Antipattern for Extensions
    internal class ServiceLocator : IHostedService
    {
        public static IServiceProvider? ServiceProvider { get; private set; }

        public ServiceLocator(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }

        Task IHostedService.StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        Task IHostedService.StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
