﻿using FluentValidation;
using Studio8Test.GrpcServices;

namespace Studio8Test.Validators
{
    public class SimpleMathFunctionValidator : AbstractValidator<SimpleMathFunction>
    {
        public SimpleMathFunctionValidator()
        {
            RuleFor(f => f.Number)
                .GreaterThan(0)
                .WithMessage("The number should be greater than 0")
                .LessThan(50)
                .WithMessage("The number should be less than 50")
                .Must(n => (long)n % 2 == 0)
                .WithMessage("The number must be even");

            RuleFor(f => f.Function)
                .Must(f => f.Equals("cube", System.StringComparison.InvariantCultureIgnoreCase))
                .WithMessage("The word should be \"cube\""); ;
        }
    }
}
