using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

namespace Studio8Test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging((ctx, loggingBuilder) =>
                {
                    var cfg = new LoggerConfiguration()
                        .ReadFrom.Configuration(ctx.Configuration)
                        .Enrich.FromLogContext()
                        .Enrich.WithProperty("ApplicationContext", "ServerApp")
                        .WriteTo.Console(
                            theme: AnsiConsoleTheme.Code,
                            outputTemplate: "[{Timestamp:HH:mm:ss.fff} {Level:u3} {SourceContext}] {Message:lj}{NewLine}{Exception}");

                    var seqServerUrl = ctx.Configuration["Serilog:Seq:ServerUrl"];
                    if (!string.IsNullOrWhiteSpace(seqServerUrl))
                        cfg.WriteTo.Seq(seqServerUrl, apiKey: ctx.Configuration["Serilog:Seq:ApiKey"], compact: true);

                    Log.Logger = cfg.CreateLogger();
                    loggingBuilder.ClearProviders();
                    loggingBuilder.AddSerilog(dispose: true);
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}