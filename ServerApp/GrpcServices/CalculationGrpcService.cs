using System.Threading.Tasks;
using Grpc.Core;
using Studio8Test.Tools.Calculation;

namespace Studio8Test.GrpcServices
{
    public class CalculationGrpcService : CalculationGrpc.CalculationGrpcBase
    {
        private readonly ICalculator _calculator;

        public CalculationGrpcService(ICalculator calculator)
        {
            _calculator = calculator;
        }

        public override Task<CalculationReply> Calculate(SimpleMathFunction simpleMathFunc, ServerCallContext context)
            => context.ProcessGrpcMessage(simpleMathFunc, (msg, ctx)
                => new CalculationReply { Result = _calculator.Calculate(msg.Number, msg.Function) });
    }
}