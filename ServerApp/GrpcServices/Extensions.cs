﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Studio8Test.GrpcServices
{
    public static class Extensions
    {
        private static ILogger _logger { get; } = ServiceLocator.ServiceProvider.GetRequiredService<ILoggerProvider>().CreateLogger("ProcessGrpcMessage");
        private static IHostEnvironment _hostEnvironment { get; } = ServiceLocator.ServiceProvider.GetRequiredService<IHostEnvironment>();

        public static Task<TReply> ProcessGrpcMessage<TMessage, TReply>(this ServerCallContext context, TMessage message, Func<TMessage, ServerCallContext, TReply> processFunc)
            where TMessage : IMessage<TMessage>
            where TReply : IMessage<TReply>
        {
            return Task.Run(() =>
            {
                var validator = ServiceLocator.ServiceProvider.GetService<IValidator<TMessage>>(); //Need pool
                var validationResult = validator.Validate(message);
                if (validationResult.IsValid)
                {
                    try
                    {
                        return processFunc(message, context);
                    }
                    catch (Exception exc)
                    {
                        _logger.LogError(exc, "Error processing grpc message");
                        var statusDetails = _hostEnvironment.IsDevelopment() ? exc.Message : "Internal error in message processing";
                        context.Status = new Status(StatusCode.Internal, statusDetails);
                    }
                }
                else
                    context.Status = new Status(StatusCode.InvalidArgument, validationResult.ToString());
                return Activator.CreateInstance<TReply>();
            });
        }
    }
}
